import mysql.connector
from clase.cd import *

class AdminCd:
        def __init__(self):
                self.__cnx = mysql.connector.connect(user='root', password='root',host='localhost',database='coleccion_cds')
                
                
        def insertarCd(self):
                cursor = self.__cnx.cursor()
                
                cd = input("Introduce un CD: \n ")
                fecha = input("Introduce una fecha de compra (aaaa-mm-dd): \n ")
                query = ("INSERT INTO cd (nombre,fecha_compra) VALUES ('%s','%s')" % (cd,fecha)) #Aquí va la query de SQL
                
                cursor.execute(query)                
                self.__cnx.commit()
                cursor.close()


        def actualizarCd(self,pId,pCd):

                #query actualizar los datos con la id anterior
                cursor = self.__cnx.cursor()
                query = ("UPDATE cd SET nombre = '%s' WHERE id_cd = %i" %(pCd,pId)) #Aquí va la query de SQL
                cursor.execute(query)
                self.__cnx.commit()
                cursor.close()

        def listarCd(self):
                #query listar generos por id
                cursor = self.__cnx.cursor()
                query = ("SELECT id_cd,nombre,fecha_compra FROM cd") #Aquí va la query de SQL
                cursor.execute(query)
                cds = cursor.fetchall()
                listacds=[]

                for cd in cds:
                        listacds.append(Cd(cd[0],cd[1],cd[2]))

                return listacds
                cursor.close()

        def borrarCd(self, pId):
                cursor = self.__cnx.cursor()
                query = ("DELETE FROM cd WHERE id_cd = %i" % pId) #Aquí va la query de SQL
                cursor.execute(query)
                self.__cnx.commit()
                cursor.close()
