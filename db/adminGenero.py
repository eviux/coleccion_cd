

import mysql.connector

class AdminGenero:
        def __init__(self):
                self.__cnx = mysql.connector.connect(user='root', password='root',host='localhost',database='coleccion_cds')
                
                
        def insertarGenero(self):
                cursor = self.__cnx.cursor()
                
                genero = input("Introduce un genero: \n ")
                query = ("INSERT INTO genero (nombre) VALUES ('%s')" % genero) #Aquí va la query de SQL
                cursor.execute(query)
                self.__cnx.commit()

                cursor.close()


        def actualizarGenero(self,pId,pGenero):

                #query actualizar los datos con la id anterior
                cursor = self.__cnx.cursor()
                query = ("UPDATE genero SET nombre = '%s' WHERE id_genero = %i" %(pGenero,pId)) #Aquí va la query de SQL
                cursor.execute(query)
                self.__cnx.commit()
                cursor.close()

        def listarGenero(self):
                #query listar generos por id
                cursor = self.__cnx.cursor()
                queryid = ("SELECT id_genero,nombre FROM genero") #Aquí va la query de SQL
                cursor.execute(queryid)
                generos = cursor.fetchall()
                listaGeneros=[]
                for genero in generos:
                        listaGeneros.append(genero)

                return listaGeneros

                cursor.close()

        def getGeneroById(self):
                #query listar generos por id
                cursor = self.__cnx.cursor()
                id_cd = int(input("Introduce el ID del CD del que quieres ver su género o géneros: "))
                queryid = ("SELECT nombre FROM genero WHERE id_genero = '%i'" % id_cd) #Aquí va la query de SQL
                cursor.execute(queryid)
                generos = cursor.fetchall()
                listaGeneros=[]
                for genero in generos:
                        listaGeneros.append(genero)

                return listaGeneros

                cursor.close()

        def borrarGenero(self, pIdGenero):
                cursor = self.__cnx.cursor()
                query = ("DELETE FROM genero WHERE id_genero = %i" % pIdGenero) #Aquí va la query de SQL
                cursor.execute(query)
                self.__cnx.commit()
                cursor.close()

                
