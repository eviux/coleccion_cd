

import mysql.connector
from clase.discografica import *

class AdminDiscografica:
        def __init__(self):
                self.__cnx = mysql.connector.connect(user='root', password='root',host='localhost',database='coleccion_cds')
                
                
        def insertarDiscografica(self):
                cursor = self.__cnx.cursor()
                
                discografica = input("Introduce una discográfica: \n ")
                query = ("INSERT INTO discografica (nombre) VALUES ('%s')" % discografica) #Aquí va la query de SQL
                cursor.execute(query)
                self.__cnx.commit()

                cursor.close()


        def actualizarDiscografica(self,pId,pDiscografica):

                #query actualizar los datos con la id anterior
                cursor = self.__cnx.cursor()
                query = ("UPDATE discografica SET nombre = '%s' WHERE id_discografica = %i" %(pDiscografica,pId)) #Aquí va la query de SQL
                cursor.execute(query)
                self.__cnx.commit()
                cursor.close()

        def listarDiscografica(self):
                #query listar generos por id
                cursor = self.__cnx.cursor()
                query = ("SELECT id_discografica,nombre FROM discografica") #Aquí va la query de SQL
                cursor.execute(query)
                discograficas = cursor.fetchall()
                listaDiscografica=[]
                '''
                for genero in discograficas:
                        listaGeneros.append(genero)
                '''
                for discografica in discograficas:
                        listaDiscografica.append(Discografica(discografica[0],discografica[1]))

                return listaDiscografica
                cursor.close()

        def borrarDiscografica(self, pIdDiscografica):
                cursor = self.__cnx.cursor()
                query = ("DELETE FROM discografica WHERE id_discografica = %i" % pIdDiscografica) #Aquí va la query de SQL
                cursor.execute(query)
                self.__cnx.commit()
                cursor.close()

                
