class Cd:
	def __init__(self, pId, pNombre, pFecha):
		self.SetNombre(pNombre)
		self.SetId(pId)
		self.SetFecha(pFecha)

	def SetNombre(self, pNombre):
		self.__nombre = pNombre

	def GetNombre(self):
		return self.__nombre

	def SetId(self, pId):
		self.__id = pId

	def GetId(self):
		return self.__id

	def SetFecha(self, pFecha):
		self.__fecha = pFecha

	def GetFecha(self):
		return self.__fecha

	def __str__ (self):
                return str(self.__id) +" "+ str(self.__nombre) + ". Año: " + (str(self.__fecha.strftime("%Y")))
