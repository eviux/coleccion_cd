class Discografica:
	def __init__(self, pId, pNombre):
		self.SetNombre(pNombre)
		self.SetId(pId)

	def SetNombre(self, pNombre):
		self.__nombre = pNombre

	def GetNombre(self):
		return self.__nombre

	def SetId(self, pId):
		self.__id = pId

	def GetId(self):
		return self.__id

	def __str__ (self):
		return str(self.__id) +" "+ str(self.__nombre )


