from clase.genero import *
from db.adminGenero import *
from db.adminDiscografica import *
from db.adminCd import *

def menu():
	print(" 		1.- Administrar género\n 		2.- Administrar discográfica\n 		3.- Administrar CD\n 		0.- Salir\n")

def subMenu():
	print(" 		1.- Insertar\n 		2.- Actualizar\n 		3.- Listar\n 		4.- Borrar\n 		5.- Menú principal\n")

opcion = 'q'

while(opcion != '0'):

	menu()
	opcion = input("Selecciona una opción: ")

	if(opcion == '1'):
		subMenu()
		selectedQuery = input("Selecciona la consulta que quieres realizar: \n ")

		if(selectedQuery == '1'):

			adminGenero = AdminGenero()
			adminGenero.insertarGenero()

		elif (selectedQuery == '2'):
			
			adminGenero = AdminGenero()
			listageneros = adminGenero.listarGenero()
			
			for genero in listageneros:
				print(str(genero[0]) +" "+ str(genero[1]))
			
			idGenero = int(input("Introduce una id: "))
			updategenero = input("Introduce un género:")
			adminGenero.actualizarGenero(idGenero,updategenero)

		elif (selectedQuery == '3'):
			
			adminGenero = AdminGenero()
			listageneros = adminGenero.listarGenero()
			
			for genero in listageneros:
				print(str(genero[0]) +" "+ str(genero[1]))
		
		elif (selectedQuery == '4'):

			adminGenero = AdminGenero()
			listageneros = adminGenero.listarGenero()

			for genero in listageneros:
				print(str(genero[0]) +" "+ str(genero[1]))
			
			idGenero = int(input("Introduce una id: "))
			adminGenero.borrarGenero(idGenero)
		elif(selectedQuery == '5'):
			continue
		else:
			print ("Error, no has elegido ninguna opcion válida")

	if(opcion == '2'):
		subMenu()
		selectedQuery = input("Selecciona la consulta que quieres realizar: \n ")

		if(selectedQuery == '1'):

			adminDiscografica = AdminDiscografica()
			adminDiscografica.insertarDiscografica()

		elif (selectedQuery == '2'):
			
			adminDiscografica = AdminDiscografica()
			listadiscograficas = adminDiscografica.listarDiscografica()
			
			for discografica in listadiscograficas:
				print(discografica)
				#print(str(discografica[0]) +" "+ str(discografica[1]))
			
			idDiscografica = int(input("Introduce una id: "))
			updatediscografica = input("Introduce una discográfica:")
			adminDiscografica.actualizarDiscografica(idDiscografica,updatediscografica)
		
		elif (selectedQuery == '3'):
			
			adminDiscografica = AdminDiscografica()
			listadiscograficas = adminDiscografica.listarDiscografica()
			
			for discografica in listadiscograficas:
				print(discografica)
			
			print(" ")
		elif (selectedQuery == '4'):

			adminDiscografica = AdminDiscografica()
			listadiscograficas = adminDiscografica.listarDiscografica()

			for discografica in listadiscograficas:
				print(discografica)
			try:
				idDiscografica = int(input("Introduce una id: "))
				adminDiscografica.borrarDiscografica(idDiscografica)
			except:
				print("No se puede eliminar esa discográfica")
		elif(selectedQuery == '5'):
			continue		
		else:
			print ("Error, no has elegido ninguna opcion válida")

	if(opcion == '3'):
		subMenu()
		selectedQuery = input("Selecciona la consulta que quieres realizar: \n ")

		if(selectedQuery == '1'):

			adminCd = AdminCd()
			adminCd.insertarCd()

		elif (selectedQuery == '2'):
			
			adminCd = AdminCd()
			listacds = adminCd.listarCd()

			for cd in listacds:
				print(cd)
			
			idCd = int(input("Introduce una id: "))
			updatecd = input("Introduce un CD:")
			adminCd.actualizarCd(idCd,updatecd)
		
		elif (selectedQuery == '3'):
			
			adminCd = AdminCd()
			adminGenero = AdminGenero()
			listacds = adminCd.listarCd()

			for cd in listacds:
				print(cd)
				
			listageneros = adminGenero.getGeneroById()

			for genero in listageneros:
				print(genero)
				
		
		elif (selectedQuery == '4'):

			adminCd = AdminCd()
			listacds = adminCd.listarCd()

			for cd in listacds:
				print(cd)
			
			idCd = int(input("Introduce una id: "))
			adminCd.borrarCd(idCd)
		elif(selectedQuery == '5'):
			continue		
		else:
			print ("Error, no has elegido ninguna opcion válida")
print("Saliendo....")
